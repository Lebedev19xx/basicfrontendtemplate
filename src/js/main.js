let uu = re('url-unshort')();

uu.expand('http://goo.gl/HwUfwd')
    .then(url => {
        if (url) console.log('Original url is: ${url}');
        // no shortening service or an unknown one is used
        else console.log('This url can\'t be expanded');
    })
    .catch(err => console.log(err));