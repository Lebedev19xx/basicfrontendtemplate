/** Плагины для установки
 https://www.npmjs.com/package/run-sequence
 https://www.npmjs.com/package/merge-stream
 https://www.npmjs.com/package/gulp.spritesmith
 https://www.npmjs.com/package/gulp-uglify
 https://www.npmjs.com/package/gulp-pug
 https://www.npmjs.com/package/pump
 https://www.npmjs.com/package/gulp-rename
 https://www.npmjs.com/package/gulp-imagemin
 https://www.npmjs.com/package/posthtml-include
 https://www.npmjs.com/package/gulp-posthtml
 https://www.npmjs.com/package/del
 https://www.npmjs.com/package/gulp-plumber/
 https://www.npmjs.com/package/gulp-sass/
 https://www.npmjs.com/package/gulp-csso/
 https://www.npmjs.com/package/gulp-autoprefixer
 https://www.browsersync.io/
 */


import fs from 'fs';
import path from 'path';

import gulp from 'gulp';

// Load all gulp plugins automatically
// and attach them to the `plugins` object
import plugins from 'gulp-load-plugins';

// Temporary solution until gulp 4
// https://github.com/gulpjs/gulp/issues/355

import archiver from 'archiver';

import glob from 'glob';
import del from 'del';
import ssri from 'ssri';
import modernizr from 'modernizr';

import sass from 'gulp-sass';
import plumber from 'gulp-plumber';
import autoprefixer from 'gulp-autoprefixer';
import server from 'browser-sync';
import minify from 'gulp-csso';
import imagemin from 'gulp-imagemin';
import rename from 'gulp-rename';
import pump from 'pump';
import uglify from 'gulp-uglify';
import pug from 'gulp-pug';
import run from 'run-sequence';
import spritesmith from 'gulp.spritesmith';
import merge from 'merge-stream';
import posthtml from 'gulp-posthtml';
import include from 'posthtml-include';
import cleanCSS from 'gulp-clean-css';
import fileSize from 'gulp-filesize';

import pkg from './package.json';
import modernizrConfig from './modernizr-config.json';


const dirs = pkg['h5bp-configs'].directories;



// ---------------------------------------------------------------------
// | Helper tasks                                                      |
// ---------------------------------------------------------------------



gulp.task("server", function () {
    server.init({
        server: "dist/",
        notify: false,
        open: true,
        cors: true,
        ui: false
    });
    // Check for changes when the server is running
    gulp.watch("src/scss/**/*.scss", ["css"]).on("change", server.reload);
    gulp.watch("src/img/sprite/*.png", ["sprite", "css"]).on("change", server.reload);
    gulp.watch("src/img/*.*", ["images"]).on("change", server.reload);
    gulp.watch("src/js/**/*.js", ["compress"]).on("change", server.reload);
    gulp.watch("src/**/*.html", ["html"]).on("change", server.reload);
    gulp.watch("src/**/*.pug", ["pug"]).on("change", server.reload);
});

// Compiling pug in html, checking plumber and posthtml
gulp.task('html', function () {
    return gulp.src('src/*.html')
        .pipe(plumber())
        .pipe(posthtml([
            include()
        ]))
        .pipe(gulp.dest("dist"));
});
gulp.task('pug', function () {
  return gulp.src('src/pug/*.pug')
  .pipe(pug({
    pretty: true
  }))
  .pipe(plumber())
  .pipe(posthtml([
      include()
    ]))
  .pipe(gulp.dest("dist"));
  });


// Compiling scss in css, checking postcss and autoprefixer, minimizing
gulp.task("css", function () {
    gulp.src("src/scss/main.scss")
        .pipe(fileSize())
        .pipe(plumber())
        .pipe(sass())
        .pipe(rename("style.css"))
        .pipe(gulp.dest("dist/css"))
        .pipe(gulp.dest("src/css"))
        .pipe(cleanCSS())
        .pipe(rename("style.min.css"))
        .pipe(gulp.dest("src/css"))
        .pipe(gulp.dest("dist/css"))
        .pipe(fileSize())
        .pipe(server.stream());
});

// Compress the pictures
gulp.task("images", function () {
    return gulp.src("src/img/*.{jpg,png}")
        .pipe(imagemin([
            imagemin.optipng({optimizationLevel: 3}),
            imagemin.jpegtran({progressive: true})
        ]))
        .pipe(gulp.dest("dist/img"));
});

// Making a PNG sprite
gulp.task('sprite', function () {
    var spriteData = gulp.src('src/img/sprite/*.png')
        .pipe(spritesmith({
            imgName: 'sprite.png',
            cssName: 'sprite.scss',
        }));
    var imgStream = spriteData.img
        .pipe(gulp.dest("dist/img"));
    return merge(imgStream);
});

gulp.task('compress:js', function (cb) {
    pump([
            gulp.src('src/js/*.js'),
            uglify(),
            gulp.dest('dist/js')
        ],
        cb
    );
});

// Clear the /DIST/ directory
gulp.task("clean:dist", function () {
    return del("dist");
});

gulp.task('archive:zip', (done) => {
  fs.mkdirSync(path.resolve(dirs.archive), '0755');
  const archiveName = path.resolve(dirs.archive, `${pkg.name}_v${pkg.version}.zip`);
  const zip = archiver('zip');
  const files = glob.sync('**/*.*', {
    'cwd': dirs.dist,
    'dot': true // include hidden files
  });
  const output = fs.createWriteStream(archiveName);

  zip.on('error', (error) => {
    done();
    throw error;
  });

  output.on('close', done);

  files.forEach((file) => {

    const filePath = path.resolve(dirs.dist, file);

    // `zip.bulk` does not maintain the file
    // permissions, so we need to add files individually
    zip.append(fs.createReadStream(filePath), {
      'name': file,
      'mode': fs.statSync(filePath).mode
    });

  });

  zip.pipe(output);
  zip.finalize();

});

gulp.task('copy', [
  'copy:.htaccess',
  'copy:index.html',
  'copy:jquery',
  'copy:license',
  'copy:main.css',
  'copy:misc',
  'copy:bootstrap 4',
  'copy:fonts',
  'copy:modernizr'
  // 'copy:normalize'
]);

gulp.task('copy:.htaccess', () =>
  gulp.src('node_modules/apache-server-configs/dist/.htaccess')
    .pipe(plugins().replace(/# ErrorDocument/g, 'ErrorDocument'))
    .pipe(gulp.dest(dirs.dist))
);

gulp.task('copy:index.html', () => {
  const hash = ssri.fromData(
    fs.readFileSync('node_modules/jquery/dist/jquery.min.js'),
    {algorithms: ['sha256']}
  );
  let version = pkg.devDependencies.jquery;
  let modernizrVersion = pkg.devDependencies.modernizr;

  gulp.src(`${dirs.src}/index.html`)
    .pipe(plugins().replace(/{{JQUERY_VERSION}}/g, version))
    .pipe(plugins().replace(/{{MODERNIZR_VERSION}}/g, modernizrVersion))
    .pipe(plugins().replace(/{{JQUERY_SRI_HASH}}/g, hash.toString()))
    .pipe(gulp.dest(dirs.dist));
});

gulp.task("copy:fonts", function () {
    return gulp.src([
        "src/fonts/**/*.{woff,woff2,eot,ttf}"
    ], {
        base: "./src"
    })
        .pipe(gulp.dest("dist"));
});

gulp.task('copy:jquery', () =>
  gulp.src(['node_modules/jquery/dist/jquery.min.js'])
    .pipe(plugins().rename(`jquery-${pkg.devDependencies.jquery}.min.js`))
    .pipe(gulp.dest(`${dirs.dist}/js/vendor`))
);

gulp.task('copy:bootstrap 4', function (){
    gulp.src(['node_modules/bootstrap/dist/css/*.css'])
        .pipe(gulp.dest(`${dirs.dist}/css`))
        .pipe(gulp.dest(`${dirs.src}/css`));
    gulp.src(['node_modules/bootstrap/dist/js/*.js'])
        .pipe(gulp.dest(`${dirs.dist}/js/vendor`))
        .pipe(gulp.dest(`${dirs.src}/js/vendor`));
});

gulp.task('copy:license', () =>
  gulp.src('LICENSE.txt')
    .pipe(gulp.dest(dirs.dist))
);

gulp.task('copy:main.css', () => {

  const banner = `/*! HTML5 Boilerplate v${pkg.version} | ${pkg.license} License | ${pkg.homepage} */\n\n`;

  gulp.src(`${dirs.src}/css/main.css`)
    .pipe(plugins().header(banner))
    .pipe(plugins().autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9', '> 1%'],
      cascade: false
    }))
    .pipe(gulp.dest(`${dirs.dist}/css`));
});

gulp.task('copy:misc', () =>
  gulp.src([

    // Copy all files
    `${dirs.src}/**/*`,

    // Exclude the following files
    // (other tasks will handle the copying of these files)
      `!${dirs.src}/css/main.css`,
      `!${dirs.src}/scss`,
      `!${dirs.src}/scss/**/*.scss`,
      `!${dirs.src}/index.html`
  ], {

    // Include hidden files by default
    dot: true

  }).pipe(gulp.dest(dirs.dist))
);

gulp.task('copy:normalize', () =>
  gulp.src('node_modules/normalize.scss/normalize.scss')
    .pipe(gulp.dest(`${dirs.dist}/css`))
);

gulp.task('copy:modernizr', (done) =>{
  modernizr.build(modernizrConfig, (code) => {
    fs.writeFile(`${dirs.dist}/js/vendor/modernizr-${pkg.devDependencies.modernizr}.min.js`, code, done);
  });

});


// ---------------------------------------------------------------------
// | Main tasks                                                        |
// ---------------------------------------------------------------------

// Compile the project
gulp.task("build", function (done) {
    run(
        "clean:dist",
        "images",
        "sprite",
        "css",
        "html",
        "compress:js",
        "copy",
        done
    );
});

gulp.task('archive', (done) => {
  run(
    'build',
    'archive:create_archive_dir',
    'archive:zip',
    done);
});

gulp.task('default', ['dist']);
