# Basic Frontend Template 

### 1. Шаблон включает в себя:

- HTML 5 Boilerplate `v 6.1.0`
- Gulp.js `v 3.9.1`
- Bootstrap 4 `v 4.1.3` 
- Font-Awesome Free `v 5.4.1`

---

### 2. Установленные пакеты для gulp.js:

- Run-sequence `v 2.2.1` `npm i run-sequence` 
- Merge-stream `v 1.0.1` `npm i merge-stream`
- Gulp.spritesmith `v 6.9.0` `npm i gulp.spritesmith`
- Gulp-uglify `v 3.0.1` `npm i gulp-uglify`
- Gulp-pug `v 4.0.1` `npm i gulp-pug`
- Pump `v 3.0.0` `npm i pump`
- Gulp-rename `v 1.4.0` `npm i gulp-rename`
- Gulp-imagemin `v 4.1.0` `npm i gulp-imagemin`
- Posthtml-include `v 1.2.0` `npm i posthtml-include`
- Gulp-posthtml `v 3.0.4` `npm i gulp-posthtml`
- Del `v 3.0.0` `npm i del`
- Gulp-plumber `v 1.2.0` `npm i gulp-plumber`
- Gulp-sass `v 4.0.2` `npm i gulp-sass`
- Gulp-csso `v 3.0.1` `npm i gulp-csso`
- Gulp-autoprefixer `v 6.0.0` `npm i gulp-autoprefixer`
- Browsersync `v 2.x` Global Install `npm install -g browser-sync` Local Install `npm install browser-sync --save-dev`
- Bootstrap 4 `v 4.1.3` `npm i bootstrap`
---

### 3. Основная структура проекта
-------------------

```
├── dir/                # Статика для проекта
    ├── css/            # Готовые файлы стилей для всех основных браузеров                
    ├── doc/            # Документация       
    ├── fonts/          # Шрифты       
    ├── img/            # Картинки для проекта       
    ├── js/             # Файлы скриптов javascript
        └── vendor/     # Файлы скриптов сторонних библиотек js
├── node_modules/       # Дериктория node модулей

├── src/                
    ├── css/            # Файлы стилей для удобной разработки     
    ├── doc/            # Документация    
    ├── fonts/          # Файлы шрифтов   
    ├── img/            # Картинки для проекта
        └── sprite/     # PNG-изображения для спрайта         
    ├── js/             # Файлы скриптов javascript
        └── vendor/     # Файлы скриптов сторонних библиотек js
    ├── scss/           # Файлы предпроцессора Sass
├── test/       
        
           
```